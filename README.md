* Applied  RoutePrefix for the controllers for easy maintenance ot resources
* Introduce Data Access Project and used Entity Framework to simplify data access.
* This improves solution maintainability, Defence against SQL injection and transaction support. 
* Moving database connection string to web.config to ease the configuration management when deploying to different environments
* Introduce Domain Project to keep models and Service project to keep the services. This improves structure and maintainability of the solution (Separation of concerns). 
* Introduce dependency injection using Ninject. Improves handling of dependencies, reusability and testability’
* Add Swagger for API documentation and testing. 

Notes:
* The same model objects will be used in Data Access and Api project for simplicity. However domain model objects and Api mode objects can be separate if required to handle the complexity. 
* Api versioning is not considers in this scenario for simplicity. API versioning is highly recommended in real world scenarios especially for public APIs to minimize the breaking changes
