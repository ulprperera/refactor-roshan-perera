﻿using refactor_this.Domain.Models;
using System.Data.Entity;

namespace refactor_this.DataAccess
{
    public class RefactorContext : DbContext, IRefactorContext
    {
        public RefactorContext() : base("Refactor")
        {
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

        }
    }
}
