﻿using refactor_this.Domain.Models;
using System.Data.Entity;

namespace refactor_this.DataAccess
{
    public interface IRefactorContext
    {
        DbSet<Account> Accounts { get; set; }
        DbSet<Transaction> Transactions { get; set; }
        int SaveChanges();
    }
}
