﻿using System;

namespace refactor_this.Domain.Models
{
    public class Transaction
    {
        public Guid Id { get; set; } 
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public Guid AccountId { get; set; }       
    }
}