﻿using refactor_this.DataAccess;
using refactor_this.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;


namespace refactor_this.Service
{
    public class AccountService : IAccountService
    {
        private readonly IRefactorContext _Context;

        public AccountService(IRefactorContext Context)
        {
            _Context = Context;
        }

        public void DeleteAccount(Guid id)
        {
            var account = _Context.Accounts.First(x => x.Id == id);
            _Context.Accounts.Remove(account);
            _Context.SaveChanges();
        }

        public Account GetAccount(Guid id)
        {
            return _Context.Accounts.First(x => x.Id == id);
        }

        public IEnumerable<Account> GetAllAccounts()
        {
            return _Context.Accounts.ToList();          
        }

        public Account SaveAccount(Account account)
        {
            _Context.Accounts.AddOrUpdate(account);
            _Context.SaveChanges();
            return account;
        }
    }
}
