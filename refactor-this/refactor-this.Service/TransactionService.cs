﻿using System;
using System.Linq;
using refactor_this.Domain.Models;
using refactor_this.DataAccess;
using System.Collections.Generic;

namespace refactor_this.Service
{
    public class TransactionService : ITransactionService
    {
        private readonly IRefactorContext _context;

        public TransactionService(IRefactorContext Context)
        {
            _context = Context;
        }

        public void AddTransaction(Transaction transaction)
        {
            var account = _context.Accounts.FirstOrDefault(x => x.Id == transaction.AccountId);
            if (account == null)
            {
                throw new Exception("Invalid account Id");
            }

            account.Amount += transaction.Amount;
            transaction.Id = Guid.NewGuid();
            _context.Transactions.Add(transaction);
            _context.SaveChanges();
        }

        public IEnumerable<Transaction> GetTransactionsByAccountId(Guid accountId)
        {
            return _context.Transactions.Where(x => x.AccountId == accountId);
        }        
    }
}