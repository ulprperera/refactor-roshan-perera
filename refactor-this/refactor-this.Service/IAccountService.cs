﻿using refactor_this.Domain.Models;
using System;
using System.Collections.Generic;


namespace refactor_this.Service
{
    public interface IAccountService
    {
        Account GetAccount(Guid id);
        IEnumerable<Account> GetAllAccounts();
        Account SaveAccount(Account account);
        void DeleteAccount(Guid id);
    }
}
