﻿using refactor_this.Domain.Models;
using System;
using System.Collections.Generic;

namespace refactor_this.Service
{
    public interface ITransactionService
    {
        IEnumerable<Transaction> GetTransactionsByAccountId(Guid accountId);
        void AddTransaction(Transaction transaction);
    }
}
