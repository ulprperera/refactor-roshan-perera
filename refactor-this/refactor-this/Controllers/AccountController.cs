﻿using refactor_this.Domain.Models;
using refactor_this.Service;
using System;
using System.Web.Http;

namespace refactor_this.Controllers
{
    /// <summary>
    /// Accounts
    /// </summary>
    [RoutePrefix("api/Accounts")]
    public class AccountController : ApiController
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        /// <summary>
        /// Gets an account by the specified id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("{id}")]
        public IHttpActionResult GetById(Guid id)
        {
            return Ok(_accountService.GetAccount(id));
        }

        /// <summary>
        ///  Gets the list of all accounts
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("")]
        public IHttpActionResult GetList()
        {
            return Ok(_accountService.GetAllAccounts());
        }

        /// <summary>
        /// Creates a new account
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPost, Route("")]        
        public IHttpActionResult Add(Account account)
        {
            return Ok(_accountService.SaveAccount(account));
        }

        /// <summary>
        ///  Updates an account
        /// </summary>
        /// <param name="id"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        [HttpPut, Route("{id}")]
        public IHttpActionResult Update(Guid id, Account account)
        {
            account.Id = id;
            return Ok(_accountService.SaveAccount(account));
        }

        /// <summary>
        /// Deletes an account
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete, Route("{id}")]
        public IHttpActionResult Delete(Guid id)
        {
            _accountService.DeleteAccount(id);
            return Ok();
        }
    }
    
}