﻿using refactor_this.Domain.Models;
using refactor_this.Service;
using System;
using System.Web.Http;

namespace refactor_this.Controllers
{
    /// <summary>
    /// Transactions
    /// </summary>
    [RoutePrefix("api/Accounts/{accountId}/Transactions")]
    public class TransactionController : ApiController
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        /// <summary>
        /// Gets the list of transactions for an account
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("")]
        public IHttpActionResult GetTransactions(Guid accountId)
        {
            return Ok(_transactionService.GetTransactionsByAccountId(accountId));
        }

        /// <summary>
        ///  Adds a transaction to an account, and updates the amount of money in the account
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="transaction"></param>
        /// <returns></returns>
        [HttpPost, Route("")]
        public IHttpActionResult AddTransaction(Guid accountId, Transaction transaction)
        {
            transaction.AccountId = accountId;
            _transactionService.AddTransaction(transaction);
            return Ok();
        }
    }
}